﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wallet2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        WALLLETEntities context;
        Wallet walletWindow;
        User data;
        Signup signup;
        Login loginWindow;

        public MainWindow()
        {
            InitializeComponent();
            Application.Current.MainWindow = this;
            context = new WALLLETEntities();
            loginWindow = new Login();
            signup = new Signup();
            loginWindow.LoginBtn.Click += LoginButton_Click;
            signup.okBtn.Click += OkBtn_Click;
            loginWindow.ShowDialog();
            this.Close();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            string loginText = loginWindow.login.Text;
            string passwordText = loginWindow.password.Password;
            data = (from item in context.Users
                    where item.login == loginText
                    && item.password == passwordText
                    select item).FirstOrDefault();

            if (data == null)
            {
                MessageBox.Show("Please Sign Up");
                loginWindow.Hide();
                this.Close();
                signup.ShowDialog();
            }
            else
            {
                walletWindow = new Wallet();

                walletWindow.luys.Content = data.electricy + " AMD";
                walletWindow.gaz.Content = data.gas + " AMD";
                walletWindow.phone.Content = data.phone + " AMD";
                walletWindow.water.Content = data.water + " AMD";
                walletWindow.cart.Content = data.amount + " AMD";

                walletWindow.payLuys.Click += PayLuys_Click;
                walletWindow.payGaz.Click += PayGaz_Click;
                walletWindow.payPhone.Click += PayPhone_Click;
                walletWindow.payWater.Click += PayWater_Click;
                loginWindow.Close();
                walletWindow.ShowDialog();
            }
        }
        private void OkBtn_Click(object sender, RoutedEventArgs e)
        {
            string login = signup.login.Text;
            string password = signup.password.Password;

            Random rand = new Random();
            context.Users.Add(new User()
            {
                login = login,
                password = password,
                electricy = rand.Next(1000, 20000),
                gas = rand.Next(1000, 10000),
                water = rand.Next(1000, 5000),
                phone = rand.Next(1000, 4000),
                amount = rand.Next(5000, 80000)
            });
            context.SaveChanges();
            MessageBox.Show("Successfully signed up, try to login!");

            signup.Hide();
            this.Close();
            //loginWindow.Owner = this;
            loginWindow.ShowDialog();
        }


        private void PayGaz_Click(object sender, RoutedEventArgs e)
        {
            if (data.amount >= data.gas)
            {
                data.amount -= data.gas;
                data.gas = 0;
                //context.SaveChanges();


            }
            else
            {
                MessageBox.Show("NOT ENOUGH MONEY!");
            }

            UpdateScreen();
        }
        private void PayWater_Click(object sender, RoutedEventArgs e)
        {
            if (data.amount >= data.water)
            {
                data.amount -= data.water;
                data.water = 0;
                //context.SaveChanges();


            }
            else
            {
                MessageBox.Show("NOT ENOUGH MONEY!");
            }

            UpdateScreen();
        }
        private void PayPhone_Click(object sender, RoutedEventArgs e)
        {
            if (data.amount >= data.phone)
            {
                data.amount -= data.phone;
                data.phone = 0;
                //context.SaveChanges();


            }
            else
            {
                MessageBox.Show("NOT ENOUGH MONEY!");
            }

            UpdateScreen();
        }

        private void PayLuys_Click(object sender, RoutedEventArgs e)
        {
            if (data.amount >= data.electricy)
            {
                data.amount -= data.electricy;
                data.electricy = 0;
                //context.SaveChanges();

            }
            else
            {
                MessageBox.Show("NOT ENOUGH MONEY!");
            }

            UpdateScreen();
        }

        private void UpdateScreen()
        {
            walletWindow.cart.Content = data.amount + " AMD";
            walletWindow.Content = data.electricy + " AMD";
            walletWindow.gaz.Content = data.gas + " AMD";
            walletWindow.phone.Content = data.phone + " AMD";
            walletWindow.water.Content = data.water + " AMD";
            this.Close();
        }

    }
}
